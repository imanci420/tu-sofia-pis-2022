#!/bin/bash
set -e
set -x;

export PROJECT_ID=$(gcloud config get-value core/project)

# NOTE: this doesn't work on MacOS. Instead, please make those changes in the "k8s/gitlab-runner-config.yaml" directly
#sed -i "s/GITLAB_RUNNER_TOKEN/${GITLAB_RUNNER_TOKEN}/g" "k8s/gitlab-runner-config.yaml"
#sed -i "s/PROJECT_ID/${PROJECT_ID}/g" "k8s/gitlab-runner-config.yaml"

kubectl create namespace gitlab || true
kubectl apply -f k8s --namespace gitlab

