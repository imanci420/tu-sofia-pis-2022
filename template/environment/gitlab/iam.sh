#!/bin/bash

set -e
export PROJECT_ID=$(gcloud config get-value core/project)

########################################################################################################################
export UNIQUE_IDENTIFIER="Нещо уникално за проекта, например '<gitlab group>-<gitlab project name>'"
export NAMESPACE="gitlab-${UNIQUE_IDENTIFIER}"
########################################################################################################################

export SERVICE_ACCOUNT_NAME="gitlab-runner-${UNIQUE_IDENTIFIER}";
export SERVICE_ACCOUNT_EMAIL="${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"

gcloud iam service-accounts create ${SERVICE_ACCOUNT_NAME} --display-name "GitLab Runner ${UNIQUE_IDENTIFIER}" || true;

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
       --member="serviceAccount:${SERVICE_ACCOUNT_EMAIL}" \
       --role='roles/owner' || echo "Failed to give service account permissions (needs owner permissions), do this manually";

kubectl create namespace ${NAMESPACE} || true

if kubectl describe secret google-key --namespace=${NAMESPACE} >/dev/null ; then
    echo -n "";
else
    gcloud iam service-accounts keys create \
       --iam-account "${SERVICE_ACCOUNT_EMAIL}" \
       service-account.json

    kubectl create secret generic google-key --from-file service-account.json --namespace=${NAMESPACE}
fi;

if kubectl get clusterrolebinding ${NAMESPACE}-cluster-admin; then
    echo -n ""
else
    kubectl create clusterrolebinding ${NAMESPACE}-cluster-admin --clusterrole=cluster-admin --serviceaccount=${NAMESPACE}:default
fi;
