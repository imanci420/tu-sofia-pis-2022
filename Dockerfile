FROM openjdk:8-jre
ADD target/tu-sofia-pis-2022-0.0.1-SNAPSHOT.jar /
ENTRYPOINT ["java", "-jar", "/tu-sofia-pis-2022-0.0.1-SNAPSHOT.jar"]
